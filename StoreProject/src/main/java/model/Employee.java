/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nitro 5
 */
public class Employee {
    private int id;
    private String name;
    private String tel;
    private String status;
    private Double salary;

    public Employee(int id, String name, String tel, String status, Double salary) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.status = status;
        this.salary = salary;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public String getStatus() {
        return status;
    }

    public Double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Empolyee{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", status=" + status + ", salary=" + salary + '}';
    }
    

    
    
}
