/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Yumat
 */
public class Customer {

    private int id;
    private String name;
    private String tel;
    private int point;
    private String status;

    public Customer(int id, String name, String tel, int point, String status) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.point = point;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public int getPoint() {
        return point;
    }

    public String getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTel(String tel) {
        if (tel.length() == 10 && status.equals("Yes")) {
            this.tel = tel;
        }
        if (!status.equals("Yes")) {
            this.tel = "-";
        }
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "customer{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", point= " + point + ", status=" + status + '}';
    }

}
