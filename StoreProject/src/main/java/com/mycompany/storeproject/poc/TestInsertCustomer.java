/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Yumat
 */
public class TestInsertCustomer {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO customer (name,tel,point,status) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Customer customer = new Customer(-1,"Yumat","0878564586",87,"y");
            stmt.setString(1,customer.getName());
            stmt.setString(2,customer.getTel());
            stmt.setInt(3,customer.getPoint());
             stmt.setString(4,customer.getStatus());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affec row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}